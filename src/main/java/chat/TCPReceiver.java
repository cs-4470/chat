package chat;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;

public class TCPReceiver implements Runnable
{
    private boolean runReceiver;
    private Socket socket;
    private TCPManager manager;

    /**
     * The receiver that will be listening for messages.
     * 
     * @param Socket socket - the socket to listen to.
     * @param TCPManager manager - The manager that manages all the sockets.
     */
    public TCPReceiver(Socket socket, TCPManager manager)
    {
        runReceiver = true;
        this.socket = socket;
        this.manager = manager;
    }

    @Override
    public void run()
    {
        while(runReceiver)
        {
            try
            {
                receiveMessage();
            }
            catch (IOException e)
            {
                System.out.println("Broke out of receive call!");
                this.runReceiver = false;
            }
        }
    }

    /**
     * Will block as it waits to receive a message on the socket.
     * 
     * @throws IOException - if something occurs to the socket that it is listening to
     */
    public synchronized void receiveMessage() throws IOException
    {
        DataInputStream dataInput = new DataInputStream(socket.getInputStream());
        int messageSize = dataInput.readInt();
        byte[] buffer = new byte[messageSize];
        dataInput.readFully(buffer);
        String message = new String(buffer);
        if(message.equalsIgnoreCase("terminate me!!!!!"))
        {
            System.out.println("Message received from " + socket.getInetAddress().getHostAddress());
            System.out.println("Sender's port: " + socket.getPort());
            System.out.println("Received termination signal from this user.");
            this.manager.terminate(socket);
        }
        else
        {
            System.out.println("Message received from " + socket.getInetAddress().getHostAddress());
            System.out.println("Sender's port: " + socket.getPort());
            System.out.println("Message: \"" + message + "\"");
        }
    }

    /**
     * shutdowns the receiver thread. Technically not needed.
     * 
     */
    public void shutdown()
    {
        this.runReceiver = false;
    }
}
