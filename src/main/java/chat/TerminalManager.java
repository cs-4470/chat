package chat;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;

public class TerminalManager implements Runnable
{
    private boolean runTerminal;
    private TCPManager tcpManager;
    private TCPClient client;
    private TCPServer server;
    private ExecutorService executor;

    /**
     * Manages the terminal of the chat application. It will parse inputs of the user and
     * act accordingly to the commands.
     * 
     * @param TCPManager tcpManager - This is the socket manager that handles the list of connections
     * @param TCPClient client - The client of the application
     * @param ExecutorService executor - executor service of the running threads
     */
    public TerminalManager(TCPManager tcpManager, TCPClient client, TCPServer server, ExecutorService executor)
    {
        this.runTerminal = true;
        this.tcpManager = tcpManager;
        this.client = client;
        this.server = server;
        this.executor = executor;
    }

    @Override
    public void run()
    {
        Scanner terminal = new Scanner(System.in);  // Create a Scanner object
        while(runTerminal)
        {
            System.out.print("\nchat>");

            String userInput = terminal.nextLine();  // Read user input
            String[] args = userInput.split(" ");
            this.parseArgs(args);
        }
        System.out.println("Goodbye!!!");
        terminal.close();
        executor.shutdownNow();
    }

    /**
     * Parses the commands of the user.
     * 
     * @param String[] args - input provided by the user in the terminal
     */
    public void parseArgs(String[] args)
    {
        if(args[0].equalsIgnoreCase("connect"))
        {
            if(args.length < 3)
            {
                System.out.println("Not enough arguments were given!");
                return;
            }
            connectCommand(args[1], args[2]);
        }
        else if(args[0].equalsIgnoreCase("myip"))
        {
            System.out.println(this.client.getUserIP());
        }
        else if(args[0].equalsIgnoreCase("myport"))
        {
            System.out.println(this.client.getUserPort());
        }
        else if(args[0].equalsIgnoreCase("help"))
        {
            System.out.println(helpCommand());
        }
        else if(args[0].equalsIgnoreCase("list"))
        {
            tcpManager.listConnections();
        }
        else if(args[0].equalsIgnoreCase("send"))
        {
            if(args.length < 3)
            {
                System.out.println("Not enough arguments were given!");
                return;
            }
            try
            {
                StringBuilder message = new StringBuilder();
                int id = Integer.parseInt(args[1]);
                for(int index = 2; index < args.length; ++index)
                {
                    message.append(args[index]);
                    message.append(" ");
                }
                if(message.length() > 100)
                {
                    System.out.println("Error! Message is too big! Message can only be up to 100 characters long!");
                    return;
                }
                client.send(id, message.toString());
            }
            catch (NumberFormatException e)
            {
                System.out.println("You did not provide a valid id!");
            }
        }
        else if(args[0].equalsIgnoreCase("terminate"))
        {
            if(args.length < 2)
            {
                System.out.println("Not enough arguments were given!");
                return;
            }
            try
            {
                int id = Integer.parseInt(args[1]);
                this.client.send(id, "terminate me!!!!!");
                tcpManager.terminate(id);
                System.out.println("Connection terminated!");
            }
            catch (NumberFormatException e)
            {
                System.out.println("You did not provide a valid number!");
            }
        }
        else if(args[0].equalsIgnoreCase("exit"))
        {
            this.runTerminal = false;
            this.client.exit();
            this.tcpManager.shutdown();
            this.server.shutdown();
        }
        else
        {
            System.out.println("Invalid command given! Type \"help\" for the list of commands.");
        }
    }

    /**
     * The function that creates the "help" that is outputted to the user
     * 
     */
    public String helpCommand()
    {
        StringBuilder message = new StringBuilder();
        message.append("\n");
        message.append("help: ");
        message.append("Displays information about the available user interface options or command manual.");
        message.append("\n");

        message.append("\n");
        message.append("myip: ");
        message.append("Displays the IP address of this process.");
        message.append("\n");

        message.append("\n");
        message.append("myport: ");
        message.append("Displays the port on which this process is listening for incoming connections.");
        message.append("\n");

        message.append("\n");
        message.append("connect <destination> <port no>: ");
        message.append("This command establishes a new TCP connection to the specified" +
        " <destination> at the specified <port no>. The <destination> is the IP address of the computer.");
        message.append("\n");

        message.append("\n");
        message.append("list: ");
        message.append("Displays a numbered list of all the connections this process is part of.");
        message.append("\n");

        message.append("\n");
        message.append("terminate <connection id>: ");
        message.append("This command will terminate the connection listed under the specified id when LIST is used to display all connections.");
        message.append("\n");

        message.append("\n");
        message.append("send <connection id> <message>: ");
        message.append("Sends a message up to 100 characters to the specified id.");
        message.append("\n");

        message.append("\n");
        message.append("exit: ");
        message.append("Closes all connections and terminates this process.");
        message.append("\n");

        return message.toString();
    }

    /**
     * The function that is called when the user calls connect in the terminal.
     * 
     * @param String ipAddress - The ip address to connect to
     * @param String port - The port to connect to
     */
    public void connectCommand(String ipAddress, String port)
    {
        InetAddress connectingAddress;
        int connectingPort;
        try
        {
            connectingAddress = InetAddress.getByName(ipAddress);
            connectingPort = Integer.parseInt(port);
            if(ipAddress.equals(InetAddress.getLocalHost().getHostAddress()) && connectingPort == client.getUserPort())
            {
                System.out.println("Error! You are attempting a self connection!");
                return;
            }
        }
        catch(UnknownHostException u)
        {
            System.out.println("Error! Invalid ip address was given");
            return;
        }
        catch(NumberFormatException n)
        {
            System.out.println("Error! Invalid port was given.");
            return;
        }
        client.connect(connectingAddress, connectingPort);
    }
    
}
