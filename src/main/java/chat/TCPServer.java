package chat;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class TCPServer implements Runnable
{
    private boolean runServer;
    private int port;
    private TCPManager tcpManager;
    private ServerSocket serverSocket;

    /**
     * Creates the server that will be waiting for new connections from other clients
     * 
     * @param int port - the port to listen on
     * @param TCPManager tcpManager - This is the socket manager that handles the list of connections
     */
    public TCPServer(int port, TCPManager tcpManager)
    {
        runServer = true;
        this.port = port;
        this.tcpManager = tcpManager;
    }

    @Override
    public void run()
    {
        try
        {
            this.serverSocket = new ServerSocket(this.port);
        }
        catch(IOException e)
        {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            System.out.println(sw.toString());
            //System.out.println("Failed connection attempt occurred!");
            this.runServer = false;
        }
        while(runServer)
        {
            this.createNewConnection();
        }
    }

    /**
     * This function will block as it waits for a new connection from a client.
     * 
     */
    private synchronized void createNewConnection()
    {
        Socket socket;
        try
        {
            socket = this.serverSocket.accept();
            tcpManager.addSocket(socket);
            System.out.println("Connected to " + socket.getInetAddress().getHostAddress() + ":" + socket.getPort());
        }
        catch(IOException e)
        {
            // StringWriter sw = new StringWriter();
            // e.printStackTrace(new PrintWriter(sw));
            // System.out.println(sw.toString());
            System.out.println("Broke out of accept call!");
            socket = null;
            //this.runServer = false;
        }
    }

    /**
     * Shutdowns the server by closing the serverSocket the server is listening on
     * 
     */
    public void shutdown()
    {
        this.runServer = false;
        try 
        {
            this.serverSocket.close();
        }
        catch (IOException e)
        {
        }
    }
}