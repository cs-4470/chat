package chat;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicReference;

public class TCPClient
{
    private int userPort;
    private AtomicReference<ArrayList<Socket>> socketList;
    private TCPManager tcpManager;

    /**
     * Creates the client that will connect to servers and send messages to the servers
     * 
     * @param int userPort - the port the client is on
     * @param AtomicReference<ArrayList<Socket>> socketList - the list of sockets that have been created
     * @param TCPManager tcpManager - This is the socket manager that handles the list of connections
     */
    public TCPClient(int userPort, AtomicReference<ArrayList<Socket>> socketList, TCPManager tcpManager)
    {
        this.userPort = userPort;
        this.socketList = socketList;
        this.tcpManager = tcpManager;
    }

    /**
     * Attempts to create a new connection to the specified ip address and port
     * 
     * @param InetAddress ipAddress - the ip address the server
     * @param int port - the port the server is listening on
     */
    public synchronized boolean connect(InetAddress ipAddress, int port)
    {
        try
        {
            for(int index = 0; index < this.socketList.get().size(); ++index)
            {
            Socket csocket = this.socketList.get().get(index);
            int newSocketPort = csocket.getPort();
            String connectingIP= ipAddress.toString();
            String indexSocket= csocket.getInetAddress().getHostAddress();
            indexSocket= "/" + indexSocket;
                if(indexSocket.equalsIgnoreCase(connectingIP) && newSocketPort==port )
                {
                    System.out.printf("Duplicate connection, please try again.\t");
                    return false; 

                }
            }
            
            System.out.println("Connecting to " + ipAddress + ":" + port + " ...");
            Socket socket = new Socket(ipAddress, port);
            this.tcpManager.addSocket(socket);
            //System.out.println("Successfully connected to " + ipAddress + ":" + port);
            return true;
        }
        catch (Exception e)
        {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            System.out.println(sw.toString());
            System.out.println("Failed to connected to " + ipAddress + ":" + port);
            return false;
        }
    }

    /**
     * Sends a message to the specified server via its id.
     * 
     * @param int id - the id of the server that is on the list of connections
     * @param String message - the message that will be sent over
     */
    public synchronized boolean send(int id, String message)
    {
        try
        {
            Socket socket = socketList.get().get(id);
            OutputStream outToConnection = socket.getOutputStream();
            DataOutputStream out = new DataOutputStream(outToConnection);
            byte[] bytes = message.getBytes();
            out.writeInt(bytes.length);
            out.write(bytes);
            if(message.equalsIgnoreCase("terminate me!!!!!"))
            {
                System.out.println("Terminating connection to " + id);
            }
            else
            {
                System.out.println("Message sent to " + id);
            }
            return true;
        }
        catch(IOException e)
        {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            System.out.println(sw.toString());
            return false;
        }
        catch(IndexOutOfBoundsException outOfBounds)
        {
            System.out.println("The id provided was greater than the amount of connections there are!");
            return false;
        }
    }

    /**
     * Used to send a termination signal to all of the clients connections.
     * 
     */
    public synchronized void exit()
    {
        if(this.socketList.get().size() > 0)
        {
            for(int index = 0; index < this.socketList.get().size(); ++index)
            {
                this.send(index, "terminate me!!!!!");
            }
        }
    }

    /**
     * Returns the user's ip address
     * 
     */
    public String getUserIP()
    {
        try
        {
            return InetAddress.getLocalHost().getHostAddress();
        }
        catch(UnknownHostException u)
        {
            return null;
        }
    }
    
    /**
     * Returns the user's port
     * 
     */
    public int getUserPort()
    {
        return userPort;
    }
}
