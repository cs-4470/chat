package chat;

import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicReference;

public class Chat
{
    public static void main(String args[])
    {
        try
        {
            int userPort = Integer.parseInt(args[0]);
            if(userPort < 1 || userPort > 65535)
            {
                System.out.println("Number provided is either too small or big!");
                return;
            }
            ExecutorService executor = Executors.newFixedThreadPool(2);
            ArrayList<Socket> list = new ArrayList<>();
            AtomicReference<ArrayList<Socket>> socketList = new AtomicReference<ArrayList<Socket>>(list);
            TCPManager tcpManager = new TCPManager(socketList);
            TCPServer tcpServer = new TCPServer(userPort, tcpManager);
            TCPClient client = new TCPClient(userPort, socketList, tcpManager);
            TerminalManager terminal = new TerminalManager(tcpManager, client, tcpServer, executor);
            executor.execute(terminal);
            executor.execute(tcpServer);
        }
        catch(NumberFormatException e)
        {
            System.out.println("You did not provide a valid port number!");
        }
    }
}