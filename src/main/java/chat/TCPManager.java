package chat;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicReference;

public class TCPManager
{
    private AtomicReference<ArrayList<Socket>> socketList;
    private Map<Socket, TCPReceiver> tcpDataMap;
    private ExecutorService receiverExe;

    /**
     * The socket manager that handles the sockets and the receiver threads
     * 
     * @param AtomicReference<ArrayList<Socket>> socketList - The initialized list of sockets
     */
    public TCPManager(AtomicReference<ArrayList<Socket>> socketList)
    {
        this.socketList = socketList;
        this.tcpDataMap = new HashMap<>();
        this.receiverExe = Executors.newCachedThreadPool();
    }

    /**
     * Adds the newly created socket to the socket list
     * 
     * @param Socket socket - The newly created socket
     */
    public synchronized boolean addSocket(Socket socket)
    {
        boolean status = false;
        if(socket != null)
        {
            status = this.socketList.get().add(socket);
            this.createReceiver(socket);
        }
        System.out.println("Successfully connected to " + socket.getInetAddress().getHostAddress() + ":" + socket.getPort());
        return status;
    }

    /**
     * Creates a new receiver thread for the newly created socket
     * 
     * @param Socket socket - The newly created socket
     */
    private void createReceiver(Socket socket)
    {
        TCPReceiver receiver = new TCPReceiver(socket, this);
        this.tcpDataMap.put(socket, receiver);
        this.tcpDataMap.containsKey(socket);
        this.receiverExe.execute(receiver);
    }

    /**
     * Closes the socket, specified by id, and receiver thread associated with the socket.
     * The socket is also removed from the list of sockets. Called when the user wishes to
     * terminate a connection
     * 
     * @param int id - The newly created socket
     */
    public synchronized boolean terminate(int id)
    {
        try
        {
            Socket socket = socketList.get().remove(id);
            if(this.tcpDataMap.containsKey(socket))
            {
                TCPReceiver receiver = this.tcpDataMap.remove(socket);
                receiver.shutdown();
                socket.close();
                return true;
            }
            socket.close();
            return true;
        }
        catch(IOException e)
        {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            System.out.println(sw.toString());
            return false;
        }
        catch(IndexOutOfBoundsException outOfBounds)
        {
            StringWriter sw = new StringWriter();
            outOfBounds.printStackTrace(new PrintWriter(sw));
            System.out.println(sw.toString());
            return false;
        }
    }

    /**
     * Closes the socket specified and receiver thread associated with the socket.
     * The socket is also removed from the list of sockets. Called when a terminate signal
     * is received from a client
     * 
     * @param int id - The newly created socket
     */
    public synchronized boolean terminate(Socket socket)
    {
        try
        {
            if(socketList.get().remove(socket) && this.tcpDataMap.containsKey(socket))
            {
                TCPReceiver receiver = this.tcpDataMap.remove(socket);
                receiver.shutdown();
                socket.close();
                return true;
            }
            return false;
        }
        catch(IOException e)
        {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            System.out.println(sw.toString());
            return false;
        }
        catch(IndexOutOfBoundsException outOfBounds)
        {
            StringWriter sw = new StringWriter();
            outOfBounds.printStackTrace(new PrintWriter(sw));
            System.out.println(sw.toString());
            return false;
        }
    }

    /**
     * Prints out the list of connections that the program has
     * 
     */
    public void listConnections()
    {
        ArrayList<Socket> list = this.socketList.get();
        if(list.size() == 0)
        {
            System.out.println("You have no connections!");
            return;
        }
        System.out.println("ID:\tIP Address:\t\tPort No:");
        for(int index = 0; index < list.size(); ++index)
        {
            Socket socket = list.get(index);
            System.out.printf("%d:\t%s\t\t%d\n", index, socket.getInetAddress().getHostAddress(), socket.getPort());
        }
    }
    
    /**
     * Closes all sockets and receiver threads
     * 
     */
    public void shutdown()
    {
        if(this.tcpDataMap.size() > 0 && this.socketList.get().size() > 0)
        {
            for(int index = 0; index < this.socketList.get().size(); ++index)
            {
                Socket socket = this.socketList.get().get(index);
                TCPReceiver receiver = this.tcpDataMap.get(socket);
                if(receiver != null)
                {
                    receiver.shutdown();
                }
                try
                {
                    socket.close();
                }
                catch(Exception e)
                {
                    System.out.println("Erroring while shutting down in the TCPManager!");
                }
            }
        }
        this.receiverExe.shutdownNow();
    }
}
