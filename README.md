**CS 4470 Project 1: Chat**

---

## Developing Environment

1. Java 13
2. Gradle 6.6.1 - Go to https://gradle.org/install/ for installation guide

---

## Getting Started

Copy and connect the repository locally so that you can push updates you make and pull changes others make. Enter git clone and the repository URL at your command line:
git clone https://davidyard@bitbucket.org/cs-4470/chat.git

## Build Application

Go to the root directory of the project (where build.gradle is) and run "gradle clean build" in the terminal.

## Running Application

1. After building application go to the .\build\distributions and unzip the zipped folder.
2. Go inside the chat folder then go inside the bin folder and open a terminal at that location.
3. In the terminal run "chat.bat %d" (%d is the port number you wish to give the application).

### Contributions

David Yardimian - Maintained gradle (the build tool). Created the multithread design, the terminal manager that parses the user commands, and the tcp manager that maintains
the sockets that are open. Documentation of the code.

Kuong Thong - Created the server and the client. Created the receiver thread that receives messages from peers, the error handling of the program and integration of the code.
Creation of the demo video.

### Demo

https://www.youtube.com/watch?v=78j7Fszme-E